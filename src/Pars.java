import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Scanner;

public class Pars {
    ArrayList<String> StringList = new ArrayList<>();
    char[] list_1;
    int start = 0;
    int end = 0;
    ArrayList<Character> list_2 = new ArrayList<>();
    String s = "";

    public static void main(String[] args) {
        Pars p = new Pars();
        p.go();
    }

    private void go() {
        s = Generator().toString();
        Scan();
        list_1 = s.toCharArray();
        for (int i = 0; i < list_1.length; i++) {
            if ((list_1[i] == '[') && (list_1[i + 1] == '*') && (list_1[i + 2] == ']')) {
                start = i + 3;
            }
            if ((list_1[i] == '[') && (list_1[i + 1] == '/') && (list_1[i + 2] == '*') && (list_1[i + 3] == ']')) {
                end = i;
                for (int j = start; j < end; j++) {
                    list_2.add(list_1[j]);
                }
                list_2.add('\n');
            }
        }
        Print(list_2);
    }

    private ArrayList<String> Generator() {
        try {
            String URL_SITE = SourceURL();
            URL url = new URL(URL_SITE);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(url.openStream()));
            String strLine;
            while ((strLine = bufferedReader.readLine()) != null) {
                StringList.add(strLine);
            }
            System.out.println("Сайт загружен");
        } catch (IOException e) {
            System.out.println("Ошибка");
        }
        return StringList;
    }

    public String SourceURL() {
        System.out.println("Введите адрес сайта: ");
        Scanner scanner = new Scanner(System.in);
        String URL_SITE = scanner.nextLine();
        return URL_SITE;
    }

    public void Scan() {
        s = s.replace(Title_One(), "[*]");
        s = s.replace(Title_Two(), "[/*]");
    }

    public String Title_One() {
        System.out.println("Введите открывающий тег: ");
        Scanner s = new Scanner(System.in);
        String str = s.nextLine();
        return str;
    }

    public String Title_Two() {
        System.out.println("Введите закрывающий тег: ");
        Scanner s = new Scanner(System.in);
        String str = s.nextLine();
        return str;
    }

    public void Print(ArrayList<Character> list_2) {
        for (int i = 0; i < list_2.size(); i++) {
            System.out.print(list_2.get(i));
        }
    }




}